'use strict';

import { districtController } from '../controllers';

module.exports = (app) => {
    app.route('/api/districts')
        .get(districtController.findAll);
    app.route('/api/district/:id')
        .get(districtController.findOne);
    app.route('/api/district/:id')
        .delete(districtController.delete);
    app.route('/api/district')
        .post(districtController.create);
}