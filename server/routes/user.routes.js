'use strict';

import { userController } from '../controllers';
import { Authentication, RoleManagement } from '../middlewares';

module.exports = (app) => {
    app.route('/api/users')
        .get(userController.getAllUser);
    app.route('/api/users/:id')
        .get(userController.getUserById);
    app.route('/api/users/register')
        .post(userController.register);
    app.route('/api/users/login')
        .post(userController.login);
    app.route('/api/users/:id')
        .delete(userController.deleteUser);
    app.route('/api/users/:id')
        .put(userController.updateUser);
}