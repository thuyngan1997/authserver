'use strict';

import { provinceController } from '../controllers';

module.exports = (app) => {
    app.route('/api/provinces')
        .get(provinceController.findAll);
    app.route('/api/province/:id')
        .get(provinceController.findOne);
    app.route('/api/province/:id')
        .delete(provinceController.delete);
    app.route('/api/province')
        .post(provinceController.create);
}