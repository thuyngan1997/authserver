'use strict';

import { villageController } from '../controllers';

module.exports = (app) => {
    app.route('/api/villages')
        .get(villageController.findAll);
    app.route('/api/village/:id')
        .get(villageController.findOne);
    app.route('/api/village/:id')
        .delete(villageController.delete);
    app.route('/api/village')
        .post(villageController.create);
}