const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const districtSchema = Schema({
    district_name: {
        require: true,
        type: String,
        unique: true
    },
    province_id: { type: Schema.Types.ObjectId, ref: 'Province', require: true }
});

module.exports = mongoose.model('District', districtSchema);