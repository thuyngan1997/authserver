const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const provinceSchema = Schema({
    province_name: {
        require: true,
        type: String,
        unique: true
    }

});

module.exports = mongoose.model('Province', provinceSchema);