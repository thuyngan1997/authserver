const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const userSchema = new Schema({
    email: {
        type: String,
        require: true,
        unique: true
    },
    password: {
        require: true,
        type: String
    },
    username: String,
    birthday: Date,
    profession: String,
    role: {
        type: String,
        enum: ['admin', 'host', 'normal']
    },
    phone_number: String

}, {
    timestamps: true
});

module.exports = mongoose.model('User', userSchema);