const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const villageSchema = Schema({
    village_name: {
        require: true,
        type: String,
        unique: true
    },
    district_id: { type: Schema.Types.ObjectId, ref: 'District', require: true }
});

module.exports = mongoose.model('Village', villageSchema);