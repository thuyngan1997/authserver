'use strict';

import Express from 'express';
import BodyParser from 'body-parser';
import Cors from 'cors';
import FS from 'fs-extra';
import Http from 'http';
import Path from 'path';
import multer from 'multer';
var router = Express.Router();

// Configuring the database
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

//import model
require('./models/user.model');
require('./models/village.model');
require('./models/province.model');
require('./models/district.model');

const app = Express();
global.__rootDir = __dirname.replace('/server', '');

app
    .use(Cors())
    .use(BodyParser.json())
    .use(BodyParser.urlencoded({ extended: true }))
    .use(Express.static(Path.resolve(__dirname, '..', 'public'), { maxAge: 31557600000 }))
    .set('views', Path.join(__dirname, '..', 'public', 'views'))
    .set('view engine', 'ejs');

const routePath = `${__dirname}/routes/`;
FS.readdirSync(routePath).forEach((file) => {
    require(`${routePath}${file}`)(app);
});




mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.set('useCreateIndex', true)
mongoose.connect(dbConfig.remoteUrl, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});


Http.createServer(app).listen(5000, () => {
    console.log(`App listening on 5000!`);
});
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/image/uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now())
    }
});
var upload = multer({ storage: storage });
app.get('/', (req, res) => {
    res.json({
        message: "Welcome to my API",
        product: "/product",
        store: "/store",
        category: "/category"
    });
});
app.post('/fileUpload', upload.single('image'), (req, res, next) => {
    MongoClient.connect(url, (err, db) => {
        assert.equal(null, err);
        insertDocuments(db, 'public/image/uploads/' + req.file.filename, () => {
            db.close();
            res.json({ 'message': 'File uploaded successfully' });
        });
    });
});
module.exports = router;
var insertDocuments = function(db, filePath, callback) {
    var collection = db.collection('user');
    collection.insertOne({ 'imagePath': filePath }, (err, result) => {
        assert.equal(err, null);
        callback(result);
    });
}