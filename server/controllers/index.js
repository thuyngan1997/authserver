import UserController from "./user.controller";
import VillageController from "./village.controller";
import ProvinceController from "./province.controller";
import DistrictController from "./district.controller";


module.exports = {
    userController: new UserController(),
    villageController: new VillageController(),
    provinceController: new ProvinceController(),
    districtController: new DistrictController(),

};