const User = require('../models/user.model');
const bcrypt = require('bcryptjs');
import { JWTHelper } from '../helpers';

export default class UserController {
    login = async(req, res, next) => {
        try {
            let email = req.body.email;
            let password = req.body.password;
            if (email === undefined) {
                return res.json({
                    success: false,
                    error: "email is required filed"
                })
            }
            if (password === undefined) {
                return res.json({
                    success: false,
                    error: "password is required filed"
                })
            }
            const user = await User.findOne({
                email: email
            });
            if (!user) {
                console.log("user not found");
                return res.json({
                    success: false,
                    error: "Email is wrong"
                })
            }
            const isValidPassword = await bcrypt.compareSync(password, user.password);
            if (!isValidPassword) {
                return res.json({
                    success: false,
                    error: "Password is wrong"
                })
            }
            // Gen token
            const token = await JWTHelper.sign({
                id: user._id,
                username: user.username,
                role: user.role,
                profession: user.profession,
                phone_number: user.phone_number,
                email: user.email
            });
            console.log(token);
            return res.json(token);

        } catch (e) {
            console.log(e);
            return res.json({
                success: false,
                message: e.message
            })
        }
    };
    register = async(req, res) => {
        try {
            const newUser = req.body;
            if (!newUser.username || newUser.username == undefined || newUser.username == "") {
                newUser.username = newUser.email.slice(0, 10);
            }
            if (!newUser.role || newUser.role == undefined || newUser.role == "") {
                newUser.role = "normal";
            }
            if (await User.findOne({ email: newUser.email })) {
                console.log('Email is already taken!');
                return res.json({
                    success: false,
                    data: 'Email "' + newUser.email + '" is already taken'
                });
            }
            newUser.password = bcrypt.hashSync(newUser.password, 10);
            await User.create(newUser, function(err, data) {
                if (err) {
                    console.log('ERROR:', err);
                }
                console.log('Created success!', data);
                return res.status(200).json({
                    success: true,
                    data: newUser
                });
            });

        } catch (e) {
            console.log(e);
            return res.status(400).json({
                success: false,
                error: e.message
            })
        }
    };
    getUserById = async(req, res, next) => {
        try {
            const user = req.params.id;
            await User.findById(user, function(err, data) {
                if (err) {
                    console.log('ERROR:', err);
                }
                return res.status(200).json(data);
            });
        } catch (e) {
            console.log(e);
            return res.status(400).json({
                success: false,
                error: e.message
            })
        }
    };
    getAllUser = async(req, res, next) => {
        try {
            await User.find({}, function(err, data) {
                if (err) {
                    console.log('ERROR:', err);
                }
                return res.status(200).json(data);
            });
        } catch (e) {
            console.log(e);
            return res.status(400).json({
                success: false,
                error: e.message
            });
        }
    };
    deleteUser = async(req, res, next) => {
        try {
            const user = req.params.id;
            await User.deleteOne({ _id: user }, function(err) {
                if (!err) {
                    return res.status(200).json({
                        success: true,
                        data: "Delete successful!"
                    });
                }
                console.log('ERROR:', err);
            });
        } catch (e) {
            console.log(e);
            return res.status(400).json({
                success: false,
                error: e.message
            });
        }
    }
    updateUser = async(req, res, next) => {
        try {
            const user = req.params.id;
            await User.findByIdAndUpdate(user, req.body, { new: true },
                (err, data) => {
                    if (!err) {
                        return res.status(200).json(data);
                    }
                    return res.status(400).json({
                        success: false,
                        data: err.message
                    });
                });

        } catch (e) {
            console.log(e);
            return res.status(400).json({
                success: false,
                error: e.message
            });
        }
    };

};