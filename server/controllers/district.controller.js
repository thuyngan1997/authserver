const mongoose = require('mongoose');
const District = mongoose.model('District');

export default class DistrictController {
    create = async(req, res) => {
        try {
            if (!req.body) {
                return res.status(404).send({
                    message: "Please input district name"
                });
            }
            const district = await new District(req.body);
            await district.save();
            return res.status(200).json(district);
        } catch (err) {
            return res.status(500).send({
                message: err.message || "Some error occurred while creating the district."
            });
        }
    };
    findAll = async(req, res) => {
        try {
            const districts = await District.find().populate('province_id', ['province_name']);
            return res.status(200).json(districts);
        } catch (err) {
            return res.status(500).send({
                message: err.message || "Some error occurred while find districts."
            });
        };
    };

    findOne = async(req, res) => {
        try {
            const district = await District.findById(req.params.id);
            if (!district) {
                return res.status(400).json({
                    success: false,
                    error: 'district is not found'
                });
            }
            return res.status(200).json(district);
        } catch (err) {
            return res.status(500).send({
                message: "Error retrieving district with id " + req.params.id
            });
        }
    };

    delete = async(req, res) => {
        try {
            await District.findByIdAndRemove(req.params.id);
            return res.status(200).json({
                success: true,
                data: true
            })
        } catch (err) {
            return res.status(500).send({
                message: "Could not delete District with id " + req.params.id
            });
        }
    };
};