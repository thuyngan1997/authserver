const mongoose = require('mongoose');
const Village = mongoose.model('Village');

export default class VillageController {
    create = async(req, res) => {
        try {
            if (!req.body) {
                return res.status(404).send({
                    message: "Please input village name"
                });
            }
            const village = await new Village(req.body);
            await village.save();
            return res.status(200).json(village);
        } catch (err) {
            return res.status(500).send({
                message: err.message || "Some error occurred while creating the village."
            });
        }
    };
    findAll = async(req, res) => {
        try {
            const villages = await Village.find().populate("district_id", "district_name");
            return res.status(200).json(villages);
        } catch (err) {
            return res.status(500).send({
                message: err.message || "Some error occurred while find village."
            });
        };
    };

    findOne = async(req, res) => {
        try {
            const village = await Village.findById(req.params.id);
            if (!village) {
                return res.status(400).json({
                    success: false,
                    error: 'Village is not found'
                });
            }
            return res.status(200).json(village);
        } catch (err) {
            return res.status(500).send({
                message: "Error retrieving Store with id " + req.params.id
            });
        }
    };

    delete = async(req, res) => {
        try {
            const village = await Village.findByIdAndRemove(req.params.id);
            return res.status(200).json({
                success: true,
                data: true
            })
        } catch (err) {
            return res.status(500).send({
                message: "Could not delete Production with id " + req.params.id
            });
        }
    };
};