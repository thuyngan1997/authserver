const mongoose = require('mongoose');
const Province = mongoose.model('Province');

export default class ProvinceController {
    create = async(req, res) => {
        try {
            if (!req.body) {
                return res.status(404).send({
                    message: "Please input province name"
                });
            }
            const province = await new Province(req.body);
            await province.save();
            return res.status(200).json(province);
        } catch (err) {
            return res.status(500).send({
                message: err.message || "Some error occurred while creating the province."
            });
        }
    };
    findAll = async(req, res) => {
        try {
            const provinces = await Province.find();
            return res.status(200).json(provinces);
        } catch (err) {
            return res.status(500).send({
                message: err.message || "Some error occurred while find province."
            });
        };
    };

    findOne = async(req, res) => {
        try {
            const province = await Province.findById(req.params.id);
            if (!province) {
                return res.status(400).json({
                    success: false,
                    error: 'Village is not found'
                });
            }
            return res.status(200).json(province);
        } catch (err) {
            return res.status(500).send({
                message: "Error retrieving province with id " + req.params.id
            });
        }
    };

    delete = async(req, res) => {
        try {
            await Province.findByIdAndRemove(req.params.id);
            return res.status(200).json({
                success: true,
                data: true
            })
        } catch (err) {
            return res.status(500).send({
                message: "Could not delete province with id " + req.params.id
            });
        }
    };
};